# Erik Lundstedt stresstes av deltaco nätadapter tillsammans med raspberryPI 3B

### Mål
testa om den relativt billiga deltaco
nätadaptern(5V/2,4A) med USB-C(5V 4A) är likvärdig med andra nätadaptrar(5V/2,5A) gorda/sålda för Raspberry-PI
### hypotes
eftersom deltaco nätadapterns USB-A port ger 2,4A och det rekomenderade för moderna Raspberry-pi 2 och 3 är 2,5A
[enligt](https://www.raspberrypi.org/documentation/hardware/raspberrypi/power/README.md) och [enligt](https://www.raspberrypi.org/products/raspberry-pi-universal-power-supply/)
så bör deltaco nätadaptern fungera men med något sämre än den som är gord/såld för Raspberry-PI.
den bör dock prestera bättre när USB-C andvänds eftersom USB-C porten ger (4A).

### Tillvägagångssätt
jag har andvänt kommandorads verktyget stressberry utveklat för att stresstesta Raspberry-PI huvadsakligen för att testa olika system för att kyla CPUn. det bör dock fungera för ändamålet samt ge bra information och statistik.
stressberry ger som output en kombination av tid temperatur och CPU frekvens. det har även stöd för at göra om text-outputen till en linje-graf i bildformat
### resultat
Deltaco adaptern fungerar hyfsat bra så länge som man andvänder en bra kabel.
jag provade med en BILLIG kabel (köpt på poundland för circa 10kr) först(första testet med deltaco som namn) och då varnade den att strömförsörgningen var dålig med en blixt i skärmens övre högra hörn och med varningmedelanden vid bootup.


se
[resultat.md](https://gitlab.com/Erik.Lundstedt/stresstestingraspberrypi/blob/master/resultat.md)
för kortattad statistik och mer information
