## goobay
5V 2,5A
goobay nätadaptern motsvarar [denna](https://www.kjell.com/se/produkter/dator/raspberry-pi/luxorparts-natadapter-for-raspberry-pi-25-a-med-strombrytare-p88056) och är anpassad för raspberry pi
se denna som ett bra basvärde.
snitt CPU frekvensen var circa 978GHz med en snitt temperatur på 69,3°c.
starttemperatur 56,4°c.
## Deltaco
5V 2,4A
adaptern jag har testat.
det första testet gordes med en billig usb-kabel och då klagade PIen att den inte kunnde få tillräkligt hög Ampare(betecknas "A").
snitt CPU frekvensen var circa 610GHz med en snitt temperatur på 59,5°c.
starttemperatur 53,7°c.
## Deltaco med bra kabel
5V 2,4A
samma adapter bättre kabel.
snitt CPU frekvensen var circa 1027GHz med en snitt temperatur på 66,7°c.
starttemperatur 52,1°c.
## Deltaco med bra kabel och USBc adapter
5V 4A
samma adapter OCH kabel men nu via en USB-C till USB-A adapter.
snitt CPU frekvensen var circa 1011GHz med en snitt temperatur på 66,6°c.
starttemperatur 50,5°c.
